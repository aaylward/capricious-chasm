import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  site: 'https://aaylward.gitlab.io',
  base: '/capricious-chasm',
  outDir: 'public',
  publicDir: 'static',
});
